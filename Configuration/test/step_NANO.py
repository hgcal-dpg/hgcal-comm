import FWCore.ParameterSet.Config as cms

from FWCore.ParameterSet.VarParsing import VarParsing
options = VarParsing('standard')
options.register('era', None, VarParsing.multiplicity.singleton, VarParsing.varType.string,
                 "reconstruction era")
options.register('run', None, VarParsing.multiplicity.singleton, VarParsing.varType.int,
                 "run number")
options.register('skipRecHits', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
                 "skip RecHits table")
options.register('skipMeta', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
                 "skip metadata table")
options.register('skipECON', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
                 "skip ECON table")
options.register('isSimulation', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
                 "adapt for simulated samples")
options.register('modmap', None, VarParsing.multiplicity.singleton, VarParsing.varType.string,
                 "use instead this module mapper file")
options.parseArguments()

print(f'Starting NANO with era={options.era} run={options.run} skipRecHits={options.skipRecHits}')

if options.isSimulation:
    from HGCalCommissioning.Configuration.SimulationEras_cff import *
    process = initSimulationCMSProcess(procname='NANO',maxEvents=options.maxEvents, modulemapper=options.modmap)
    options.skipRecHits = True
    options.skipMeta = True
    options.skipECON = True
else:    
    from HGCalCommissioning.Configuration.SysValEras_cff import *
    process, _ = initSysValCMSProcess(procname='NANO',era=options.era, run=options.run, maxEvents=options.maxEvents, modulemapper=options.modmap)

process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.maxEvents.output = cms.optional.untracked.allowed(cms.int32,cms.PSet)

# SOURCE
process.source = cms.Source("PoolSource",
   fileNames = cms.untracked.vstring(*options.files),
   secondaryFileNames = cms.untracked.vstring()
)

# OPTIONS
process.options = cms.untracked.PSet(
    IgnoreCompletely = cms.untracked.vstring(),
    Rethrow = cms.untracked.vstring(),
    TryToContinue = cms.untracked.vstring(),
    accelerators = cms.untracked.vstring('*'),
    allowUnscheduled = cms.obsolete.untracked.bool,
    canDeleteEarly = cms.untracked.vstring(),
    deleteNonConsumedUnscheduledModules = cms.untracked.bool(True),
    dumpOptions = cms.untracked.bool(False),
    emptyRunLumiMode = cms.obsolete.untracked.string,
    eventSetup = cms.untracked.PSet(
        forceNumberOfConcurrentIOVs = cms.untracked.PSet(
            allowAnyLabel_=cms.required.untracked.uint32
        ),
        numberOfConcurrentIOVs = cms.untracked.uint32(0)
    ),
    fileMode = cms.untracked.string('FULLMERGE'),
    forceEventSetupCacheClearOnNewRun = cms.untracked.bool(False),
    holdsReferencesToDeleteEarly = cms.untracked.VPSet(),
    makeTriggerResults = cms.obsolete.untracked.bool,
    modulesToCallForTryToContinue = cms.untracked.vstring(),
    modulesToIgnoreForDeleteEarly = cms.untracked.vstring(),
    numberOfConcurrentLuminosityBlocks = cms.untracked.uint32(0),
    numberOfConcurrentRuns = cms.untracked.uint32(1),
    numberOfStreams = cms.untracked.uint32(0),
    numberOfThreads = cms.untracked.uint32(4),
    printDependencies = cms.untracked.bool(False),
    sizeOfStackForThreadsInKB = cms.optional.untracked.uint32,
    throwIfIllegalParameter = cms.untracked.bool(True),
    wantSummary = cms.untracked.bool(False)
)

# Production Info
process.configurationMetadata = cms.untracked.PSet(
    annotation = cms.untracked.string('NANO nevts:-1'),
    name = cms.untracked.string('Applications'),
    version = cms.untracked.string('$Revision: 1.19 $')
)

# Output definition
process.NANOAODoutput = cms.OutputModule("NanoAODOutputModule",
    compressionAlgorithm = cms.untracked.string('ZSTD'),
    compressionLevel = cms.untracked.int32(5),
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string('NANOAOD'),
        filterName = cms.untracked.string('')
    ),
    fileName = cms.untracked.string('NANO.root'),
    outputCommands = process.NANOAODEventContent.outputCommands
)

# Additional output definition
process.load('HGCalCommissioning.NanoTools.hgcSysValNano_cff')
process.hgCalNanoTable.skipRecHits = cms.bool(options.skipRecHits)
process.hgCalNanoTable.skipMeta = cms.bool(options.skipMeta)
process.hgCalNanoTable.skipECON = cms.bool(options.skipECON)
process.user_step = cms.Path(process.hgcSysValNanoTask)

# Path and EndPath definitions
process.endjob_step = cms.EndPath(process.endOfProcess)
process.NANOAODoutput_step = cms.EndPath(process.NANOAODoutput)

# Schedule definition
process.schedule = cms.Schedule(process.user_step,process.endjob_step,process.NANOAODoutput_step)
from PhysicsTools.PatAlgos.tools.helpers import associatePatAlgosToolsTask
associatePatAlgosToolsTask(process)

#Setup FWK for multithreaded
process.options.numberOfThreads = 4
process.options.numberOfStreams = 0

# Add early deletion of temporary data products to reduce peak memory need
from Configuration.StandardSequences.earlyDeleteSettings_cff import customiseEarlyDelete
process = customiseEarlyDelete(process)
# End adding early deletion
